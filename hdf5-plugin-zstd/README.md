# HDF5 filter plugin for the Zstd compression library.

# Configuration
The configuration should be described in a **config.mk** file.
It can be done like this:
```
ln -s config-custom.mk config.mk
```
or
```
ln -s config-system.mk config.mk
```

# Installation and testing 
```
make
sudo make install
make clean
```

