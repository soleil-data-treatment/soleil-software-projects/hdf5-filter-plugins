HDF5_INC := /usr/include/hdf5/serial/
HDF5_LIB := /usr/lib/x86_64-linux-gnu/hdf5/serial/
HDF5_LIBNAME := hdf5
HDF5_PLUGIN_PATH := /usr/lib/x86_64-linux-gnu/hdf5/plugin/

COMP_INC := /usr
COMP_LIB := /usr
COMP_LIBNAME := lz4

OUT_LIBNAME := h5lz4

CC := h5cc
H5DUMP := h5dump

CFLAGS := -g -Wall -Wextra

TEST_DIR := example
TEST_PROG := h5ex_d_lz4

RM := rm -f
INSTALL := install

