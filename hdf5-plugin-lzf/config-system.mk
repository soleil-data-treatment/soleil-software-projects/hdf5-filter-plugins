HDF5_INC := /usr/include/hdf5/serial/
HDF5_LIB := /usr/lib/x86_64-linux-gnu/hdf5/serial/
HDF5_LIBNAME := hdf5
HDF5_PLUGIN_PATH := /usr/lib/x86_64-linux-gnu/hdf5/plugin/

COMP_INC := /usr/include/liblzf
COMP_LIB := /usr/lib/x86_64-linux-gnu
COMP_LIBNAME := lzf

OUT_LIBNAME := h5lzf

CC := h5cc
H5DUMP := h5dump

CFLAGS := -g -Wall -Wextra

TEST_DIR := example
#TEST_PROG := h5ex_d_lzf

RM := rm -f
INSTALL := install

