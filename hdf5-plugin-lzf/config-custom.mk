HDF5_HOME := /opt/hdf5
HDF5_INC := $(HDF5_HOME)
HDF5_LIB := $(HDF5_HOME)
HDF5_LIBNAME := hdf5
HDF5_PLUGIN_PATH := $(HDF5_HOME)/lib/plugin/

COMP_INC := /opt/lzf/include
COMP_LIB := /opt/lzf/lib
COMP_LIBNAME := lzf

OUT_LIBNAME := h5lzf

CC := $(HDF5_HOME)/bin/h5cc
H5DUMP := $(HDF5_HOME)/bin/h5dump

CFLAGS := -g -Wall -Wextra

TEST_DIR := example
#TEST_PROG := h5ex_d_lzf

RM := rm -f
INSTALL := install

