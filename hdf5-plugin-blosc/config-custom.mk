HDF5_HOME := /opt/hdf5
HDF5_INC := $(HDF5_HOME)
HDF5_LIB := $(HDF5_HOME)
HDF5_LIBNAME := hdf5
HDF5_PLUGIN_PATH := $(HDF5_HOME)/lib/plugin/

COMP_INC := /opt/c-blosc2
COMP_LIB := /opt/c-blosc2
COMP_LIBNAME := blosc

OUT_LIBNAME := h5blosc

CC := $(HDF5_HOME)/bin/h5cc
H5DUMP := $(HDF5_HOME)/bin/h5dump

CFLAGS := -g -Wall -Wextra

TEST_DIR := example
TEST_PROG := example

RM := rm -f
INSTALL := install

